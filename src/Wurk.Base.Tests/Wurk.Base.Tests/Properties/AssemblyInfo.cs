﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "Base.Tests" )]
[assembly: AssemblyDescription( "Tests for the Wurk.Base library" )]
[assembly: AssemblyCompany( "Wurk" )]
[assembly: AssemblyProduct( "Base" )]
[assembly: AssemblyCopyright( "Copyright - Wurk, 2016" )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]

// Our versioning is provided by Anvil