using FluentAssertions;
using NUnit.Framework;
using Wurk.Base.Options.Basic.Yaml;
using Wurk.Base.Tests.Options.Examples;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Wurk.Base.Tests.Options.Basic.Yaml {
    [TestFixture]
    [TestOf( typeof(YamlOptionSerialiser) )]
    public class YamlOptionSerialiserTests {

        [SetUp]
        public void SetupHarness() => TestEnv.Reinitialise();

        private Deserializer YamlDeserialiser { get; } = GetYamlDeserialiser();

        private Serializer YamlSerialiser { get; } = GetYamlSerialiser();

        private static Deserializer GetYamlDeserialiser() {
            var builder = new DeserializerBuilder();
            builder.WithNamingConvention( new CamelCaseNamingConvention() );
            return builder.Build();
        }

        private static Serializer GetYamlSerialiser() {
            var builder = new SerializerBuilder();
            builder.WithNamingConvention( new CamelCaseNamingConvention() );
            return builder.Build();
        }


        [Test]
        [Description( "Tests that deserialisation fails correctly" )]
        public void TestOptionsFileReadFails() {

            Base.Options.Conventions.OptionSerialiser = new YamlOptionSerialiser();
            Options<ExampleTestOptions>.Current = ExampleTestOptions.Default;

            var YamlConfig = System.IO.File.ReadAllText( TestEnv.FilePath<ExampleTestOptions>( @"yaml" ) );

            var deserialisedOptions = YamlDeserialiser.Deserialize<ExampleTestOptions>( YamlConfig );
            deserialisedOptions
                .ShouldBeEquivalentTo( Options<ExampleTestOptions>.Current );
        }

        [Test]
        public void TestYamlDeserialiser() {

            Base.Options.Conventions.OptionDeserialiser = new YamlOptionSerialiser();
            var newOptions = new ExampleTestOptions {
                Name = nameof( YamlOptionSerialiser ),
                Size = 9
            };
            var serializedOptions = YamlSerialiser.Serialize( newOptions );

            System.IO.File.WriteAllText( TestEnv.FilePath<ExampleTestOptions>( @"yaml" ), serializedOptions );

            Options<ExampleTestOptions>.Current.ShouldBeEquivalentTo( newOptions );
        }


        [Test]
        public void TestYamlSerialiser() {

            Base.Options.Conventions.OptionSerialiser = new YamlOptionSerialiser();
            Options<ExampleTestOptions>.Current = ExampleTestOptions.Default;

            var YamlConfig = System.IO.File.ReadAllText( TestEnv.FilePath<ExampleTestOptions>( @"yaml" ) );

            var deserialisedOptions = YamlDeserialiser.Deserialize<ExampleTestOptions>( YamlConfig );
            deserialisedOptions.ShouldBeEquivalentTo( Options<ExampleTestOptions>.Current );


        }
    }
}