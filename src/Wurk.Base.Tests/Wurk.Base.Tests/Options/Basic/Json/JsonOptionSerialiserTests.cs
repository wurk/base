using FluentAssertions;
using Jil;
using NUnit.Framework;
using Wurk.Base.Options.Basic.Json;
using Wurk.Base.Tests.Options.Examples;

namespace Wurk.Base.Tests.Options.Basic.Json {
    [TestFixture]
    [TestOf( typeof(JsonOptionSerialiser) )]
    public class JsonOptionSerialiserTests {

        [SetUp]
        public void SetupHarness() => TestEnv.Reinitialise();

        [Test]
        public void TestJsonDeserialiser() {

            Base.Options.Conventions.OptionDeserialiser = new JsonOptionSerialiser();
            var newOptions = new ExampleTestOptions {
                Name = nameof( TestJsonSerialiser ),
                Size = 9
            };
            var serializedOptions = JSON.Serialize( newOptions, JsonOptionSerialiser.JsonOptions );
            System.IO.File.WriteAllText( TestEnv.FilePath<ExampleTestOptions>( @"json" ), serializedOptions );

            Options<ExampleTestOptions>.Current.ShouldBeEquivalentTo( newOptions );
        }

        [Test]
        public void TestJsonSerialiser() {

            Base.Options.Conventions.OptionSerialiser = new JsonOptionSerialiser();
            Options<ExampleTestOptions>.Current = ExampleTestOptions.Default;

            var jsonConfig = System.IO.File.ReadAllText( TestEnv.FilePath<ExampleTestOptions>( @"json" ) );

            var deserialisedOptions = JSON.Deserialize<ExampleTestOptions>( jsonConfig,
                JsonOptionSerialiser.JsonOptions );
            deserialisedOptions
                .ShouldBeEquivalentTo( Options<ExampleTestOptions>.Current );
        }
    }
}