﻿using System.IO;
using Wurk.Base.Options.Basic.File;

namespace Wurk.Base.Tests.Options.Basic.File.Mock {
    /// <summary>
    ///     A test configuration store, for use in <see cref="Options`1" /> unit
    ///     tests
    /// </summary>
    /// <seealso cref="T:Wurk.Base.Options.Basic.File.FileConfigurationStore" />
    public class TestConfigurationStore : FileConfigurationStore {

        public TestConfigurationStore() {
            EnsurePath( new DirectoryInfo( ConfigurationPath ) );
        }

        /// <summary>
        ///     Gets the configuration path.
        /// </summary>
        /// <value>
        ///     The configuration path.
        /// </value>
        protected override string ConfigurationPath => TestEnv.FolderPath;
    }
}