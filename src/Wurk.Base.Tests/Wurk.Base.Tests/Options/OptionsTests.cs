﻿using NUnit.Framework;
using Wurk.Base.Tests.Options.Examples;

namespace Wurk.Base.Tests.Options {


    [TestFixture]
    [TestOf( typeof(Options<>) )]
    public class OptionsTests {

        [SetUp]
        public void SetupHarness() => TestEnv.Reinitialise();

        [Test]
        [Description(
             "Ensures that the generic `Options` default property is populated with the static Default from the provided IOptions type"
         )]
        public void TestDefaultPropertyInstantiation()
            => Assert.AreEqual( Options<ExampleTestOptions>.Default.Name, nameof( ExampleTestOptions ) );


        [Test]
        [Description(
             "Ensures that simple `Options` types without a default property are populated"
         )]
        public void TestNoDefaultPropertyInstantiation()
            => Assert.AreEqual( Options<ExampleSimpleOptions>.Default.Number, 0 );

        [Test]
        [Description( "Tests that once set, options exist for a type" )]
        public void TestOptionsExistence() {
            Options<ExampleTestOptions>.Current = ExampleTestOptions.Default;
            Assert.IsTrue( Options<ExampleTestOptions>.Exists() );
        }

        [Test]
        [Description( "Tests that by default, no options exist for a type" )]
        public void TestOptionsNonExistence() {
            Assert.IsFalse( Options<ExampleTestOptions>.Exists() );
        }

        [Test]
        [Description( "Tests that once reset, options are reset to defaults" )]
        public void TestOptionsReset() {

            Options<ExampleTestOptions>.Current = new ExampleTestOptions {
                Name = nameof( TestOptionsReset ),
                Size = 88
            };

            Options<ExampleTestOptions>.Reset();

            Assert.AreNotEqual( Options<ExampleTestOptions>.Current.Name, nameof( TestOptionsReset ) );
        }
    }
}