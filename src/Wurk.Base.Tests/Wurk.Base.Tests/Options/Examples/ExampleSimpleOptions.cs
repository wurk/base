﻿using Wurk.Base.Options;

namespace Wurk.Base.Tests.Options.Examples {
    /// <summary>
    ///     A sample options type, for use in testing
    /// </summary>
    /// <seealso cref="T:Wurk.Base.Options.IOptions" />
    public struct ExampleSimpleOptions : IOptions {
        /// <summary>
        ///     Gets or sets the size.
        /// </summary>
        /// <value>
        ///     The size.
        /// </value>
        public int Number { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Nominal { get; set; }

    }
}