﻿using Wurk.Base.Options;

namespace Wurk.Base.Tests.Options.Examples {
    /// <summary>
    ///     A sample options type, for use in testing
    /// </summary>
    /// <seealso cref="T:Wurk.Base.Options.IOptions" />
    public struct ExampleTestOptions : IOptions {
        /// <summary>
        ///     Gets or sets the size.
        /// </summary>
        /// <value>
        ///     The size.
        /// </value>
        public int Size { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        ///     Gets the default.
        /// </summary>
        /// <value>
        ///     The default.
        /// </value>
        public static ExampleTestOptions Default => new ExampleTestOptions {
            Name = nameof( ExampleTestOptions ),
            Size = 7
        };
    }

}