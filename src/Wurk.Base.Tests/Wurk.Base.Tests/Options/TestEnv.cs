﻿using System;
using System.IO;
using System.Reflection;
using Wurk.Base.Options;
using Wurk.Base.Options.Utility;
using Wurk.Base.Tests.Options.Basic.File.Mock;

namespace Wurk.Base.Tests.Options {
    public static class TestEnv {

        internal static string FolderPath { get; } =
            Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ),
                $"{EntryAssembly.GetName().Name}\\test.{DateTime.UtcNow.Ticks}" );

        private static Assembly EntryAssembly => Runtime.Clr.EntryAssembly;


        internal static TestConfigurationStore ConfigurationStore { get; } = new TestConfigurationStore();


        public static void Reinitialise() {
            if ( Directory.Exists( FolderPath ) )
                DeleteAllTestOptionFiles();

            Base.Options.Conventions.ConfigurationStore = ConfigurationStore;

            // Clear our sample type from memory cache
            DictionaryCache.Clear();
        }

        /// <summary>
        ///     Deletes all test option files.
        /// </summary>
        private static void DeleteAllTestOptionFiles() {

            if ( !Directory.Exists( FolderPath ) )
                return;
            DeleteFiles( @"json" );
            DeleteFiles( @"yaml" );

        }

        private static void DeleteFiles( string ext ) {
            foreach ( var file in Directory.EnumerateFiles( FolderPath, $"*.{ext}" ) )
                File.Delete( file );
        }


        internal static string FilePath<T>( string shortName ) where T : struct, IOptions
        => Path.Combine( FolderPath, ConfigurationStore.NamingConvention.FileName<T>( shortName ) );
    }
}