﻿using System.IO;
using FluentAssertions;
using NUnit.Framework;
using Wurk.Base;

/// <summary>
/// 
/// </summary>
namespace Wurk.Base.Tests.RuntimeTests {
    public partial class RuntimeTests {

        [TestFixture]
        [TestOf(typeof(Runtime.Network))]
        public class RuntimeNetworkTests
        {

            [Test]
            public void TestHostAddress()
                => Runtime.Network.HostAddress.Should().NotBeNullOrEmpty( "the computer has a network address" );

        }
    }
}