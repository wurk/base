﻿using System.IO;
using FluentAssertions;
using NUnit.Framework;
using Wurk.Base;

namespace Wurk.Base.Tests.RuntimeTests {
    public partial class RuntimeTests {

        [TestFixture]
        [TestOf(typeof(Runtime.Testing))]
        public class RuntimeTestingTests {

            [Test]
            public void TestTestAttributes()
                => Runtime.Testing.UnitTestAttributes.Should().NotBeEmpty( "the collection has unit test attribute type names" );

        }
    }
}