﻿using System.IO;
using FluentAssertions;
using NUnit.Framework;
using Wurk.Base;

namespace Wurk.Base.Tests.RuntimeTests {
    public partial class RuntimeTests {

        [TestFixture]
        public class RuntimeExecutableTests {

            [Test]
            public void TestName()
                => Assert.AreEqual( @"Wurk.Base.Tests", Runtime.Executable.Name );

            [Test]
            public void TestPath()
                => Assert.AreEqual( true,
                    File.Exists( Path.Combine( Runtime.Executable.Path.FullName, @"Wurk.Base.Tests.dll" ) ) );

            [Test]
            public void TestWorkingPath()
                => Assert.AreEqual( true, Directory.Exists( Runtime.Executable.WorkingPath.FullName ) );

            [Test]
            public void TestVersion()
                => Runtime.Executable.Version.Should().NotBeNull( "the assembly has a version" );

        }
    }
}