﻿using NUnit.Framework;
using Wurk.Base.Values;

namespace Wurk.Base.Tests.RuntimeTests {

    [TestFixture]
    public partial class RuntimeTests {

        [Test]
        public void TestExecutionContext()
            => Assert.AreEqual( Runtime.Context, ExecutionContext.Test );

        [Test]
        public void TestRuntimeMode()
            => Assert.AreEqual( Runtime.Mode, RuntimeMode.Test );
    }
}