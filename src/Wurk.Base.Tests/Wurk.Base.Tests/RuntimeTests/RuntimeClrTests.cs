﻿using FluentAssertions;
using NUnit.Framework;

namespace Wurk.Base.Tests.RuntimeTests {
    public partial class RuntimeTests {

        [TestFixture]
        [TestOf( typeof(Runtime.Clr) )]
        public class RuntimeClrTests {

            [Test]
            public void TestAssembly()
                =>
                Runtime.Clr.Assembly.FullName.Should()
                       .StartWith( @"Wurk.Base", "this assembly contains the executing code" );

            [Test]
            public void TestExecutingAssembly()
                => Runtime.Clr.EntryAssembly.FullName.Should()
                          .StartWith( @"Wurk.Base.Tests", "this assembly is the executing assembly" );
        }
    }
}