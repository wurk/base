﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;
using Wurk.Base.Values;

namespace Wurk.Base {
    /// <summary>
    ///     An entry point for ascertaining the condition of the current executing application,
    ///     providing opinionated hooks for configuration and some limited reflection
    ///     capabilities.
    /// </summary>
    [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
    public static partial class Runtime {

        static Runtime() {

            // Using pragma here to determine defined symbols
#if (DEBUG && !TRACE)
            Mode = RuntimeMode.Development;
#elif (DEBUG && TRACE)
            Mode = RuntimeMode.Diagnostics;
#endif
            // Detect if we're in a unit test execution context
            if ( HasTestAttributesOnStack( Clr.CurrentStack() ) ) {

                Context = ExecutionContext.Test;
                Mode = RuntimeMode.Test;
#if (!DEBUG)
                // In release mode, but a Test context
                Mode = RuntimeMode.Test;
#endif
            } else {
                // TODO: Check we're actually in a service (process/service enumeration?)
                Context = Environment.UserInteractive ? ExecutionContext.Console : ExecutionContext.Service;

                Mode = Mode != RuntimeMode.Test ? RuntimeMode.Live : RuntimeMode.Test;
            }
        }

        /// <summary>
        /// Grab a stack trace, and walk the tree looking for methods decorated with our configured TestAttributes
        /// </summary>
        /// <param name="stackFrames">The stack frames.</param>
        /// <returns>
        ///   <c>true</c> if [has test attributes on stack] [the specified strack frames]; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasTestAttributesOnStack( StackFrame[] stackFrames )
            => stackFrames.Any( frame => frame.GetMethod()
                                              .DeclaringType.GetCustomAttributes( false )
                                              .Any( x => Testing.UnitTestAttributes.Contains( x.GetType().FullName ) ) );


        /// <summary>
        ///     Gets the mode.
        /// </summary>
        /// <value>
        ///     The mode.
        /// </value>
        public static RuntimeMode Mode { get; } = GetMode();

        /// <summary>
        ///     Gets the host.
        /// </summary>
        /// <value>
        ///     The host.
        /// </value>
        public static ExecutionContext Context { get; } = GetContext();

        /// <summary>
        ///     Gets the runtime mode.
        /// </summary>
        /// <returns></returns>
        private static RuntimeMode GetMode() {
            /* TODO:
             *   - Check configuration source for core options
             *   - Check execution environment for prompts (Testing, CI?)
             *   - Check for CLI flags overriding mode
             *   - Check DEBUG, TRACE, INSTRUMENT flags
*/
            return RuntimeMode.Development;
        }

        /// <summary>
        ///     Gets the runtime Environment.
        /// </summary>
        /// <returns></returns>
        private static ExecutionContext GetContext() {
            return ExecutionContext.Console;
        }

    }


}