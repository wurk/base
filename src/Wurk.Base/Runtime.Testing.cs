﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;

namespace Wurk.Base {
    /// <inheritdoc />
    public static partial class Runtime {
        /// <summary>
        ///     Provides a <see langword="static" /> entry point for CLR related
        ///     runtime data
        /// </summary>
        [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
        public static class Testing {

            public static readonly HashSet<string> UnitTestAttributes = new HashSet<string> {
                "Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute",
                "NUnit.Framework.TestFixtureAttribute"
            };

        }
    }
}