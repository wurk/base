﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "Base" )]
[assembly: AssemblyDescription( "The base library for projects in the Wurk ecosystem" )]
[assembly: AssemblyCompany( "Wurk" )]
[assembly: AssemblyProduct( "Base" )]
[assembly: AssemblyCopyright( "Copyright - Wurk, 2016" )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: Guid( "df82d109-2ee6-4bee-b2af-a7f3071270f2" )]

// Our versioning is provided by Anvil