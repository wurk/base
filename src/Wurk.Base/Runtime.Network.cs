﻿using System.Net;
using System.Net.NetworkInformation;
using JetBrains.Annotations;

namespace Wurk.Base {
    public static partial class Runtime {
        /// <summary>
        ///     Provides a static entry point for network related runtime data
        /// </summary>
        [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
        public static class Network {

            /// <summary>
            ///     Gets the host address, qualified by the domain if available
            /// </summary>
            /// <value>
            ///     The host address.
            /// </value>
            public static string HostAddress { get; } = GetHostAddress();

            /// <summary>
            ///     Gets the host address, in the domain qualified DNS form.
            /// </summary>
            /// <returns></returns>
            private static string GetHostAddress() {
                var domainName = IPGlobalProperties.GetIPGlobalProperties().DomainName;
                return $"{Dns.GetHostName()}.{domainName}".TrimEnd( '.' ).ToLower();
            }
        }
    }

}