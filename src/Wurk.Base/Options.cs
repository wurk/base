﻿using JetBrains.Annotations;
using Wurk.Base.Options;
using Wurk.Base.Options.Convention;
using Cache = Wurk.Base.Options.Utility.DictionaryCache;

namespace Wurk.Base {


    /// <summary>
    ///     Provides a <see langword="static" /> entry point for your option access
    /// </summary>
    /// <typeparam name="TOptions">
    ///     The type of the option you'd like to address
    /// </typeparam>
    /// <para>
    ///     This class provides a simple static entry point to your strongly typed
    ///     configuration classes.
    /// </para>
    [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
    public static class Options<TOptions> where TOptions : struct, IOptions {
        /* TODO: Implement Settings pattern; double-checked lazy loaded props
        Support (Embedded, File, HTTP, AMQP) -> (JSON, YAML, XML) configuration
        Configure each options source
            - Change detection (error handling, backoff, retry. Polly?)
            - Change event handler (per type!)
            - Priority
        */

        private static string TypeName => typeof(TOptions).FullName;
        private static IOptionDeserialiser Deserialiser => Conventions.OptionDeserialiser;

        private static IOptionSerialiser Serialiser => Conventions.OptionSerialiser;

        /// <summary>
        ///     Set, or retrieve current settings from cache or defaults.
        /// </summary>
        /// <value>
        ///     The current options for the provided type.
        /// </value>
        public static TOptions Current {
            get {
                return Cache.Contains( TypeName )
                    ? Cache.Retrieve<TOptions>( TypeName )
                    : Cache.Store( TypeName, Deserialiser.Read<TOptions>() );
            }
            set { Cache.Store( TypeName, Serialiser.Write( value ) ); }
        }

        /// <summary>
        ///     Gets the default options for this Options type.
        /// </summary>
        /// <value>
        ///     The Options.
        /// </value>
        public static TOptions Default {
            get {
                var value = typeof(TOptions).GetProperty( nameof( Default ) )?.GetValue( null, null );
                if ( value != null )
                    return (TOptions) value;
                return new TOptions();
            }
        }

        /// <summary>
        ///     Resets this option type to the Default
        /// </summary>
        /// <returns>
        /// The new Options
        /// </returns>
        public static TOptions Reset() => Current = Default;

        /// <summary>
        ///     Determines whether a configuration exists in the store for the given
        ///     type
        /// </summary>
        /// <returns>
        /// A boolean value indicating if a configuration was found for the provided option type
        /// </returns>
        public static bool Exists() => Cache.Contains( TypeName ) || Deserialiser.Exists<TOptions>();
    }
}