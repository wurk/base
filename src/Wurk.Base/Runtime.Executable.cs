﻿using System;
using System.IO;
using JetBrains.Annotations;

namespace Wurk.Base {
    /// <inheritdoc />
    public static partial class Runtime {
        /// <summary>
        ///     An access point to information regarding the executable for this runtime
        /// </summary>
        [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
        public static class Executable {
            /// <summary>
            ///     Gets the name of the current assembly.
            /// </summary>
            /// <value>
            ///     The name.
            /// </value>
            public static string Name { get; } = Clr.EntryAssembly?.GetName()?.Name;

            /// <summary>
            ///     Gets the path of the current runtime.
            /// </summary>
            /// <value>
            ///     The path.
            /// </value>
            public static DirectoryInfo Path { get; } = GetRuntimePath();

            /// <summary>
            ///     Gets the the location this executable was started from on the filesystem.
            /// </summary>
            /// <value>
            ///     The working path.
            /// </value>
            public static DirectoryInfo WorkingPath { get; } = GetWorkingPath();

            /// <summary>
            ///     Gets the assembly version.
            /// </summary>
            /// <value>
            ///     The assembly version.
            /// </value>
            public static Version Version { get; } = Clr.EntryAssembly?.GetName()?.Version;

            /// <summary>
            ///     Gets the working path.
            /// </summary>
            /// <returns></returns>
            private static DirectoryInfo GetWorkingPath() => new DirectoryInfo( Directory.GetCurrentDirectory() );

            /// <summary>
            ///     Gets the runtime path.
            /// </summary>
            /// <returns></returns>
            private static DirectoryInfo GetRuntimePath()
                => (Clr.EntryAssembly?.Location != null) && File.Exists( Clr.EntryAssembly?.Location )
                    ? new FileInfo( Clr.EntryAssembly.Location ).Directory
                    : new DirectoryInfo( Directory.GetCurrentDirectory() );
        }
    }

}