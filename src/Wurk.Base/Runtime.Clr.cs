﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using Wurk.Dado.Guard;

namespace Wurk.Base {
    /// <inheritdoc />
    public static partial class Runtime {


        /// <summary>
        ///     Provides a <see langword="static" /> entry point for CLR related
        ///     runtime data
        /// </summary>
        [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
        public static class Clr {

            /// <summary>
            ///     Gets the assembly currently executing.
            /// </summary>
            /// <value>
            ///     The assembly.
            /// </value>
            public static Assembly Assembly { get; } = Assembly.GetExecutingAssembly();

            /// <summary>
            ///     Gets the assembly the currently running process executed from
            /// </summary>
            /// <value>
            ///     The entry assembly.
            /// </value>
            public static Assembly EntryAssembly { get; } = Assembly.GetEntryAssembly() ?? FallbackAssemblyLocator();

            /// <summary>
            ///     Locates the entry assembly with a fallback mechanism
            /// </summary>
            /// <returns>
            /// </returns>
            private static Assembly FallbackAssemblyLocator() {

                var stackFrames = CurrentStack();

                Ensure.Instantiated( stackFrames, nameof(stackFrames) );

                var methods =
                    stackFrames.Select( t => t.GetMethod() ).OfType<MethodInfo>()?.Where( m => m != null ).ToArray();

                var invokeMethod = methods?.First( m =>

                            // Invoked via console
                                ((m.Name == "Main") && m.IsStatic &&
                                 (m.ReturnType == typeof(void)))


                                // Invoked by tests
                                || m.DeclaringType.GetCustomAttributes( false )
                                    .Any( x => Testing.UnitTestAttributes.Contains( x.GetType().FullName ) )


                                // Invoked by reflection
                                ||
                                ((m.Name == @"InvokeMethod") && (m.DeclaringType == typeof(RuntimeMethodHandle)) &&
                                 m.IsStatic)
                    );

                return (invokeMethod ?? methods.Last()).Module.Assembly;
            }


            /// <summary>
            ///     Retrieves the current method stack frames
            /// </summary>
            /// <returns>
            /// </returns>
            public static StackFrame[] CurrentStack() => new StackTrace().GetFrames();
        }
    }
}