﻿namespace Wurk.Base.Options.Convention.Store {
    /// <summary>
    ///     Describes a synchronous interface for any configuration store implementation
    ///     This pattern makes use of passing the serialisers configured, so as to allow
    ///     interop/IO based actvities to be managed by the store
    /// </summary>
    public interface IConfigurationStore {

        /// <summary>
        ///     Retrieves the specified deserialiser.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="deserialiser">The deserialiser.</param>
        /// <returns></returns>
        TOptions Retrieve<TOptions>( IOptionDeserialiser deserialiser ) where TOptions : struct, IOptions;

        /// <summary>
        ///     Persists the specified options with the provided serialiser.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="optionSerialiser">The option serialiser.</param>
        /// <param name="options">The options.</param>
        /// <returns></returns>
        TOptions Persist<TOptions>( IOptionSerialiser optionSerialiser, TOptions options )
            where TOptions : struct, IOptions;

        /// <summary>
        ///     Determines whether a configuration exists in the store for the given type, using the provided deserialiser
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="optionDeserialiser">The option deserialiser.</param>
        /// <returns></returns>
        bool Exists<TOptions>( IOptionDeserialiser optionDeserialiser ) where TOptions : struct, IOptions;
    }

}