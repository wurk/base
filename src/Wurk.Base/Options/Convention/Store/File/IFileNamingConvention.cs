﻿namespace Wurk.Base.Options.Convention.Store.File {
    /// <summary>
    /// </summary>
    public interface IFileNamingConvention {
        /// <summary>
        ///     Gets the file path of the provided options type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="shortName">The short name.</param>
        /// <returns></returns>
        string FileName<T>( string shortName ) where T : struct, IOptions;
    }
}