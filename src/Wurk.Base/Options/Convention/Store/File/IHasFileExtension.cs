﻿namespace Wurk.Base.Options.Convention.Store.File {
    public interface IHasFileExtension {
        string ShortName { get; }
    }
}