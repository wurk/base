﻿using JetBrains.Annotations;
using Wurk.Base.Options.Convention.Store.File;

namespace Wurk.Base.Options.Convention.Store {
    /// <summary>
    /// Provides an interface definition for any file backed configuration stores
    /// </summary>
    /// <seealso cref="Wurk.Base.Options.Convention.Store.IConfigurationStore" />
    public interface IFileConfigurationStore : IConfigurationStore {
        /// <summary>
        ///     Gets or sets the naming convention.
        /// </summary>
        /// <value>
        ///     The naming convention.
        /// </value>
        [UsedImplicitly]
        IFileNamingConvention NamingConvention { get; }

        /// <summary>
        ///     Gets the file path of the provided options type
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        [UsedImplicitly]
        string FilePath<TOptions>( IHasFileExtension extension ) where TOptions : struct, IOptions;
    }
}