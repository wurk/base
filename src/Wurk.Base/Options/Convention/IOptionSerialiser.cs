using System.IO;
using Wurk.Base.Options.Convention.Store.File;

namespace Wurk.Base.Options.Convention {
    /// <summary>
    /// Describes an Option Serialiser's methods, providing an extension point for our option configuration system
    /// </summary>
    /// <seealso cref="Wurk.Base.Options.Convention.Store.File.IHasFileExtension" />
    public interface IOptionSerialiser : IHasFileExtension {
        /// <summary>
        /// Writes the specified options
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="options">The options.</param>
        /// <returns></returns>
        TOptions Write<TOptions>( TOptions options ) where TOptions : struct, IOptions;

        /// <summary>
        /// Serialises the options using the specified stream writer.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="streamWriter">The stream writer.</param>
        /// <param name="options">The options.</param>
        /// <returns></returns>
        TOptions Serialise<TOptions>( StreamWriter streamWriter, TOptions options ) where TOptions : struct, IOptions;
    }
}