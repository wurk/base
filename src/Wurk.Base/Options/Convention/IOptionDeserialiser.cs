﻿using System.IO;
using Wurk.Base.Options.Convention.Store.File;

namespace Wurk.Base.Options.Convention {
    public interface IOptionDeserialiser : IHasFileExtension {

        TOptions Read<TOptions>() where TOptions : struct, IOptions;

        TOptions Deserialise<TOptions>( StreamReader streamReader ) where TOptions : struct, IOptions;

        bool Exists<TOptions>() where TOptions : struct, IOptions;
    }

}