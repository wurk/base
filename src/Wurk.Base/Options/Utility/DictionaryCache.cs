﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Wurk.Base.Options.Utility {
    /// <summary>
    ///     A simple dictionary based cache. Encapsulated for future convention overrides.
    /// </summary>
    [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
    public static class DictionaryCache {
        // Our static, in memory representation of options. This should probable be a convention item too
        private static IDictionary<string, object> Storage { get; } = new Dictionary<string, object>();

        /// <summary>
        ///     Determines whether the cache contains the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///     <c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains( string key ) => Storage.ContainsKey( key );

        /// <summary>
        ///     Retrieves the options of the provided type specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static T Retrieve<T>( string key ) => (T) Storage[key];

        /// <summary>
        ///     Stores the options at the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static T Store<T>( string key, T data ) => (T) (Storage[key] = data);

        /// <summary>
        /// Clears the storage cache
        /// </summary>
        public static void Clear() => Storage.Clear();
    }
}