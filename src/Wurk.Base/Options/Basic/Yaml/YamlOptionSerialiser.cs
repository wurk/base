using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Wurk.Base.Options.Basic.Yaml {

    /// <summary>
    /// Provides a Yaml Option Serialiser to our configuration system
    /// </summary>
    /// <seealso cref="Wurk.Base.Options.Basic.OptionSerialiserBase" />
    public class YamlOptionSerialiser : OptionSerialiserBase {

        /// <summary>
        /// Gets the yaml deserialiser.
        /// </summary>
        /// <value>
        /// The yaml deserialiser.
        /// </value>
        private static Deserializer YamlDeserialiser { get; } =
            GetYamlDeserialiser();

        /// <summary>
        /// Gets the yaml serialiser.
        /// </summary>
        /// <value>
        /// The yaml serialiser.
        /// </value>
        private static Serializer YamlSerialiser { get; } =
            GetYamlSerialiser();

        /// <summary>
        ///     Gets the short name.
        /// </summary>
        /// <value>
        ///     The short name.
        /// </value>
        public override string ShortName => "yaml";

        /// <summary>
        ///     Deserialises the specified stream reader.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="streamReader">The stream reader.</param>
        /// <returns></returns>
        public override TOptions Deserialise<TOptions>( StreamReader streamReader )
            => YamlDeserialiser.Deserialize<TOptions>( streamReader );

        /// <summary>
        ///     Serializes the specified stream writer.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="streamWriter">The stream writer.</param>
        /// <param name="options">The options.</param>
        /// <returns></returns>
        public override TOptions Serialise<TOptions>( StreamWriter streamWriter, TOptions options ) {
            YamlSerialiser.Serialize( streamWriter, options );
            return options;
        }


        private static Deserializer GetYamlDeserialiser() {
            var builder = new DeserializerBuilder();
            builder.WithNamingConvention( new CamelCaseNamingConvention() );
            return builder.Build();
        }

        private static Serializer GetYamlSerialiser() {
            var builder = new SerializerBuilder();
            builder.WithNamingConvention( new CamelCaseNamingConvention() );
            return builder.Build();
        }
    }
}