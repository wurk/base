using System.IO;
using Wurk.Base.Options.Convention;
using Wurk.Base.Options.Convention.Store;

namespace Wurk.Base.Options.Basic {
    /// <summary>
    ///     Provides an <see langword="abstract" /> base class to allow for the
    ///     simple extension and redefinition of other Serialiser types
    /// </summary>
    /// <seealso cref="T:Wurk.Base.Options.Convention.IOptionSerialiser" />
    /// <seealso cref="T:Wurk.Base.Options.Convention.IOptionDeserialiser" />
    public abstract class OptionSerialiserBase : IOptionSerialiser, IOptionDeserialiser {
        /// <summary>
        ///     A shorthand accessor method
        /// </summary>
        /// <value>
        ///     The store.
        /// </value>
        protected static IConfigurationStore Store =>
            Conventions.ConfigurationStore;

        /// <summary>
        ///     Reads this instance.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <returns>
        /// </returns>
        public TOptions Read<TOptions>() where TOptions : struct, IOptions =>
            Store.Retrieve<TOptions>( this );

        /// <summary>
        ///     Deserialises the specified stream reader.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="streamReader">The stream reader.</param>
        /// <returns>
        /// </returns>
        public abstract TOptions Deserialise<TOptions>( StreamReader streamReader ) where TOptions : struct, IOptions;

        /// <summary>
        ///     Determines whether a configuration exists in the store for the given
        ///     type
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <returns>
        /// </returns>
        public bool Exists<TOptions>() where TOptions : struct, IOptions =>
            Store.Exists<TOptions>( this );

        /// <summary>
        ///     Writes the specified options.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="options">The options.</param>
        /// <returns>
        /// </returns>
        public TOptions Write<TOptions>( TOptions options ) where TOptions : struct, IOptions =>
            Store.Persist( this, options );

        /// <summary>
        /// Serialises the passed <paramref name="options"/> with the specified stream writer.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="streamWriter">The stream writer.</param>
        /// <param name="options">The options.</param>
        /// <returns></returns>
        public abstract TOptions Serialise<TOptions>( StreamWriter streamWriter, TOptions options )
            where TOptions : struct, IOptions;

        /// <summary>
        ///     Gets the short name for this serialiser, to be used as metadata, or
        ///     for e.g., in the file extension
        /// </summary>
        /// <value>
        ///     The short name.
        /// </value>
        public abstract string ShortName { get; }
    }
}