﻿using System.IO;
using json = Jil.JSON;

namespace Wurk.Base.Options.Basic.Json {
    /// <summary>
    /// Provides a JSON Option Serialiser to our configuration system
    /// </summary>
    /// <seealso cref="T:Wurk.Base.Options.Convention.IOptionSerialiser" />
    /// <seealso cref="T:Wurk.Base.Options.Convention.IOptionDeserialiser" />
    public class JsonOptionSerialiser : OptionSerialiserBase {
        /// <summary>
        /// Gets the json options used for Jil
        /// </summary>
        /// <value>
        /// The json options.
        /// </value>
        public static Jil.Options JsonOptions { get; } = Jil.Options.ISO8601PrettyPrintExcludeNullsIncludeInheritedCamelCase;

        /// <summary>
        /// Gets the short name for this serialiser, to be used as metadata, or
        /// for e.g., in the file extension
        /// </summary>
        /// <value>
        /// The short name.
        /// </value>
        public override string ShortName { get; } = "json";

        /// <summary>
        ///     Deserialises the options using the specified stream reader.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="streamReader">The stream reader.</param>
        /// <returns></returns>
        public override TOptions Deserialise<TOptions>( StreamReader streamReader )
            =>
            json.Deserialize<TOptions>( streamReader,
                                        JsonOptions );

        /// <summary>
        ///     Serializes the options using the specified stream writer.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="streamWriter">The stream writer.</param>
        /// <param name="options">The options.</param>
        /// <returns></returns>
        public override TOptions Serialise<TOptions>( StreamWriter streamWriter, TOptions options ) {
            json.Serialize( options, streamWriter, JsonOptions );
            return options;
        }
    }
}