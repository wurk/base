﻿using Wurk.Base.Options.Convention.Store.File;

namespace Wurk.Base.Options.Basic.File {
    /// <summary>
    ///     Our default file naming convention
    /// </summary>
    /// <seealso cref="Wurk.Base.Options.Convention.Store.File.IFileNamingConvention" />
    public class DefaultFileNamingConvention : IFileNamingConvention {

        /// <summary>
        ///     Gets the file path of the provided options type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="shortName">The short name.</param>
        /// <returns></returns>
        public string FileName<T>( string shortName ) where T : struct, IOptions {
            return $"{ConfigurationName<T>()}.{shortName}";
        }

        private static string ConfigurationName<T>()
            // TODO: Replace with utils 'RemoveLastInstanceOf'
            => typeof(T).Name.Replace( nameof( Options ), string.Empty ).ToLower();
    }
}