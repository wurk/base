﻿using System;
using System.IO;
using Wurk.Base.Options.Convention;
using Wurk.Base.Options.Convention.Store;
using Wurk.Base.Options.Convention.Store.File;

namespace Wurk.Base.Options.Basic.File {
    /// <summary>
    ///     An abstract file configuration store, requiring just the setting of a base configuration path
    /// </summary>
    /// <seealso cref="IFileConfigurationStore" />
    public abstract class FileConfigurationStore : IFileConfigurationStore {

        /// <summary>
        ///     Gets the configuration path.
        /// </summary>
        /// <value>
        ///     The configuration path.
        /// </value>
        protected abstract string ConfigurationPath { get; }

        /// <summary>
        ///     Gets the naming convention in use by our FileConfigurationStore.
        ///     This is by default initialised to use the DefaultFileNamingConvention
        /// </summary>
        /// <value>
        ///     The naming convention.
        /// </value>
        public IFileNamingConvention NamingConvention { get; } = new DefaultFileNamingConvention();

        /// <summary>
        ///     Gets the file path of the provided options type
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public string FilePath<TOptions>( IHasFileExtension extension ) where TOptions : struct, IOptions
        => Path.Combine( ConfigurationPath, NamingConvention.FileName<TOptions>( extension.ShortName ) );

        /// <summary>
        ///     Retrieves the options using the specified deserialiser.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="deserialiser">The deserialiser.</param>
        /// <returns></returns>
        public TOptions Retrieve<TOptions>( IOptionDeserialiser deserialiser ) where TOptions : struct, IOptions {

            var filePath = FilePath<TOptions>( deserialiser );

            if ( !System.IO.File.Exists( filePath ) )
                return Options<TOptions>.Default;

            try {
                using ( var streamReader = new StreamReader( Stream( filePath ) ) )
                    return deserialiser.Deserialise<TOptions>( streamReader );
            } catch ( Exception e ) {
                throw new IOException("Failed to deserialise the configuration from the store", e);
//                return Options<TOptions>.Default;
                // TODO: Log and catch this somehow?
                
            }
        }

        /// <summary>
        ///     Persists the options using the specified serialiser.
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="serialiser">The serialiser.</param>
        /// <param name="options">The options.</param>
        /// <returns></returns>
        public TOptions Persist<TOptions>( IOptionSerialiser serialiser, TOptions options )
            where TOptions : struct, IOptions {

            // TODO: Rename old, write new, rename to
            var filePath = FilePath<TOptions>( serialiser );

            if ( System.IO.File.Exists( filePath ) )
                System.IO.File.Delete( filePath );
            try {

                using ( var streamWriter = new StreamWriter( Stream( filePath ) ) )
                    return serialiser.Serialise( streamWriter, options );

            } catch ( Exception e) {
                throw new IOException( "Failed to serialise the configuration provided to the store", e );
            }

        }

        protected internal static void EnsurePath( DirectoryInfo directory ) {
            if ( directory.Exists )
                return;
            try {
                Directory.CreateDirectory( directory.FullName );
            } catch ( Exception e ) {
                throw new InvalidOperationException(
                    "Cannot create the configured path for configuration storage, execution cannot continue", e );
            }
        }

        // TODO: Return post check on valid configuration/deserialisation
        /// <summary>
        /// Determines whether a configuration exists in the store for the given type, using the provided deserialiser
        /// </summary>
        /// <typeparam name="TOptions">The type of the options.</typeparam>
        /// <param name="optionDeserialiser">The option deserialiser.</param>
        /// <returns></returns>
        public bool Exists<TOptions>( IOptionDeserialiser optionDeserialiser ) where TOptions : struct, IOptions => System.IO.File.Exists( FilePath<TOptions>( optionDeserialiser ) );

        private static FileStream Stream( string path ) {
            EnsurePath( new FileInfo( path ).Directory );
            return new FileStream( path, FileMode.OpenOrCreate, FileAccess.ReadWrite );
        }
    }
}