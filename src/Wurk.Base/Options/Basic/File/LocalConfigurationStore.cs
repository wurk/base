﻿using System.IO;
using JetBrains.Annotations;

namespace Wurk.Base.Options.Basic.File {

    /// <summary>
    ///     A configuration store rooted in our application's path
    /// </summary>
    /// <seealso cref="Wurk.Base.Options.Basic.File.FileConfigurationStore" />
    [UsedImplicitly]
    public class LocalConfigurationStore : FileConfigurationStore {

        public LocalConfigurationStore(string subPath = @"conf") {
            SubPath = subPath;
        }

        /// <summary>
        /// Gets the sub path to search for options files
        /// </summary>
        /// <value>
        /// The sub path.
        /// </value>
        public string SubPath { get; }

        /// <summary>
        ///     Gets the configuration path. (/conf/ subfolder)
        /// </summary>
        /// <value>
        ///     The configuration path.
        /// </value>
        protected override string ConfigurationPath => Path.Combine( Runtime.Executable.Path.FullName, SubPath );
    }
}