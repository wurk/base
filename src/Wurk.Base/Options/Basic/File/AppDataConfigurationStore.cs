﻿using System;
using System.IO;

namespace Wurk.Base.Options.Basic.File {
    /// <summary>
    ///     An AppData rooted file store
    /// </summary>
    /// <seealso cref="FileConfigurationStore" />
    public class AppDataConfigurationStore : FileConfigurationStore {
        /// <summary>
        ///     Gets the configuration path.
        /// </summary>
        /// <value>
        ///     The configuration path.
        /// </value>
        protected override string ConfigurationPath { get; } =
            Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), Runtime.Clr.EntryAssembly?.GetName()?.Name ?? "UnknownAssembly");
    }
}