﻿using JetBrains.Annotations;
using Wurk.Base.Options.Basic.File;
using Wurk.Base.Options.Basic.Yaml;
using Wurk.Base.Options.Convention;
using Wurk.Base.Options.Convention.Store;

namespace Wurk.Base.Options {

    /// <summary>
    ///     Our conventions, configured with defaults
    /// </summary>
    public static class Conventions {
        private static readonly YamlOptionSerialiser YamlOptionSerialiser = new YamlOptionSerialiser();

        [UsedImplicitly]
        public static IOptionSerialiser OptionSerialiser { get; set; } = YamlOptionSerialiser;

        [UsedImplicitly]
        public static IOptionDeserialiser OptionDeserialiser { get; set; } = YamlOptionSerialiser;

        [UsedImplicitly]
        public static IConfigurationStore ConfigurationStore { get; set; } = new AppDataConfigurationStore();
    }

}