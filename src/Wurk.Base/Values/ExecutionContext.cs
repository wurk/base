﻿using JetBrains.Annotations;

namespace Wurk.Base.Values {
    [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
    public enum ExecutionContext {

        /// <summary>
        /// A test execution context, either in CI, or local development
        /// </summary>
        Test,

        /// <summary>
        /// Running as an interactive console, with stdout/in, etc.
        /// </summary>
        Console,

        /// <summary>
        /// Running as a windows service
        /// </summary>
        Service,

    }
}