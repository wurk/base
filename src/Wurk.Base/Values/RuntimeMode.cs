﻿using JetBrains.Annotations;

namespace Wurk.Base.Values {
    [UsedImplicitly( ImplicitUseTargetFlags.WithMembers )]
    public enum RuntimeMode {
        Development,
        Diagnostics,
        Instrumented,
        Test,
        Live
    }
}